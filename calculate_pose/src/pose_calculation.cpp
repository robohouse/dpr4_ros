#include "ros/ros.h"
#include "threemxl/Motor_vals.h"
#include "std_msgs/Header.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseStamped.h"
#include <sstream>
#include <cmath>

float x = 0.0, y = 0.0, wheelL = 0.0, wheelR = 0.0, oldWheelL = 0.0, oldWheelR = 0.0, heading = 0.0;
bool distanceReceive = false;
bool gyroReceive = true;


void distanceCallback(const threemxl::Motor_vals msg){

	ROS_INFO("x: %f", x);
	ROS_INFO("y: %f\n", y);
	//total distance received. calculating delta distance.
	wheelL = (msg.distance[0] - oldWheelL);
	wheelR = (msg.distance[1] - oldWheelR);
	oldWheelL = msg.distance[0];
	oldWheelR = msg.distance[1];

	//calculating position robot. x y and Yaw. 0.54 is width robot.
	if (fabs(wheelL - wheelR) < 1.0e-6) { // basically going straight
    		x = x + wheelL * cos(heading);
    		y = y + wheelR * sin(heading);
		heading = heading;
	} else {
		float R = 0.54 * (wheelL + wheelR) / (2 * (wheelR - wheelL)),
          	wd = (wheelR - wheelL) / 0.54;

		x = x + R * sin(wd + heading) - R * sin(heading);
		y = y - R * cos(wd + heading) + R * cos(heading);
		heading = heading + wd;
	}
	distanceReceive =true;


}


void PosePublisher(geometry_msgs::PoseStamped &pose, ros::Publisher &pose_pub){

}

int main(int argc, char **argv){
	
	ros::init(argc, argv, "Calculate_Pose");
	ros::NodeHandle n;
	geometry_msgs::PoseStamped pose;
	ros::Publisher pose_pub = n.advertise<geometry_msgs::PoseStamped>("/robot_pose", 1);
	ros::Subscriber distanceRobot = n.subscribe("motor/distance", 1, distanceCallback, ros::TransportHints().tcpNoDelay());

	ros::Rate loop_rate(20);
	
	

	while(true){
		
		
		pose.header.frame_id = "odom";
		pose.header.stamp = ros::Time::now();
		pose.pose.orientation.x = 0;
		pose.pose.orientation.y = 0;
		pose.pose.orientation.z = heading;
		pose.pose.orientation.w = 1;

		pose.pose.position.x = x;
		pose.pose.position.y = y;
		pose.pose.position.z = 0;
	
		pose_pub.publish(pose);
		
		ros::spinOnce();

		loop_rate.sleep();
	}

	return 0;
}








