#include "ros/ros.h"
#include "tf/transform_broadcaster.h"
#include "geometry_msgs/PoseStamped.h"

tf::Transform t;
tf::Quaternion q;

void pose_callback(const geometry_msgs::PoseStamped pose){

	static tf::TransformBroadcaster br;
	q.setX(pose.pose.orientation.x);
	q.setY(pose.pose.orientation.y);
	q.setZ(pose.pose.orientation.z);
	q.setW(pose.pose.orientation.w);

	t.setOrigin(tf::Vector3(pose.pose.position.x, pose.pose.position.y, 0.0));
	t.setRotation(q);

	br.sendTransform(tf::StampedTransform(t, ros::Time::now(), "odom", "base_frame"));
}

int main(int argc, char **argv){

	ros::init(argc, argv, "tf_odom_broadcast");
	ros::NodeHandle n;
	ros::Subscriber pose_sub = n.subscribe("/robot_pose", 1, pose_callback);
	ros::Rate loop_rate(100);
	while (ros::ok()){
		ros::spinOnce();
		loop_rate.sleep();
	}
}
