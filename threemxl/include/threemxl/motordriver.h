#ifndef __THREEMXL_MOTORDRIVER_H
#define __THREEMXL_MOTORDRIVER_H

#include <ros/ros.h>
#include <threemxl/platform/hardware/dynamixel/CDxlGeneric.h>
#include <threemxl/platform/hardware/dynamixel/CDxlGroup.h>
#include <geometry_msgs/Twist.h>

class DxlROSMotordriver
{
protected:
  ros::NodeHandle nh_;
  CDxlGeneric *motor_l;   ///< Motor interface
  CDxlGeneric *motor_r;   ///< Motor interface
  CDxlGroup *motors_;    ///< Multiple motor interface
  LxSerial serial_port_; ///< Serial port interface

public:
  DxlROSMotordriver();
  ~DxlROSMotordriver();

  void init(char *path);
  void spin();

  void commandCallback(const geometry_msgs::Twist::ConstPtr &msg);

private:

  //ros::Publisher distancePublishLeftMotor;
  //ros::Publisher distancePublishRightMotor;
  ros::Publisher distancePublisher;
  ros::Subscriber commandListener;
  bool stop;
  double l_speed;
  double r_speed;
  bool newCommand;
  uint8_t command;
};

#endif // MOTORDRIVER_H
