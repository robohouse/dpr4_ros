#include <stdio.h>

#include <ros/ros.h>
#include <threemxl/motordriver.h>
#include <threemxl/C3mxlROS.h>
#include <std_msgs/Float32.h>
#include <sstream>
#include <iostream>
#include <string>
#include <threemxl/Motor_vals.h>

DxlROSMotordriver::DxlROSMotordriver() : nh_("~"), motor_l(NULL), motor_r(NULL)
{

}

DxlROSMotordriver::~DxlROSMotordriver()
{
  if (motor_l)
    delete motor_l;
  if (motor_r)
    delete motor_r;

  nh_.shutdown();
}

void DxlROSMotordriver::init(char *path)
{
  CDxlConfig *config = new CDxlConfig();

  if (path)
  {
    ROS_INFO("Using shared_serial");
    motor_l = new C3mxlROS(path);
    motor_r = new C3mxlROS(path);
  }
  else
  {
    ROS_INFO("Using direct connection");
    motor_l = new C3mxl();
    motor_r = new C3mxl();

    serial_port_.port_open("/dev/ttyUSB0", LxSerial::RS485_FTDI);
    serial_port_.set_speed(LxSerial::S921600);
    motor_l->setSerialPort(&serial_port_);
    motor_r->setSerialPort(&serial_port_);
  }

  motor_l->setConfig(config->setID(106));
  motor_l->init(false);
  motor_l->set3MxlMode(SPEED_MODE);

  motor_r->setConfig(config->setID(107));
  motor_r->init(false);
  motor_r->set3MxlMode(SPEED_MODE);

  ros::start();
  ros::NodeHandle n;
  ros::NodeHandle s;
  distancePublisher = n.advertise<threemxl::Motor_vals>("motor/distance", 1000);
  //distancePublishRightMotor = n.advertise<std_msgs::Float32>("motor/right", 1000);
  //distancePublishLeftMotor = n.advertise<std_msgs::Float32>("motor/left", 1000);
  l_speed = 0;
  r_speed = 0;
  commandListener = s.subscribe("/cmd_vel", 1000, &DxlROSMotordriver::commandCallback, this);

  delete config;
}

void DxlROSMotordriver::spin()
{
  ros::Rate loop_rate(10); //herz;
  newCommand = true;
  stop = true;
  int dir = 1;
  //motor_r->setLinearPos(100, 0.0);
  //motor_l->setLinearPos(100, 0.0);
  while (ros::ok())
  {
    //motor_l->setTorque(2 * dir); // nM
    if(stop)
    {
      //motor_l->setLinearPos(0, 0.0);
      //motor_r->setLinearPos(0, 0.0);
      motor_l->setSpeed(0);
      motor_r->setSpeed(0);

      double val_r = motor_r->presentLinearPos();
      double val_l = motor_l->presentLinearPos();
      float dist_l = (float)val_l;
      float dist_r = (float)val_r;
      std::vector<float> dists;
      dists.push_back(dist_l);
      dists.push_back(dist_r);

      threemxl::Motor_vals distances;
      distances.distance = dists;


      distancePublisher.publish(distances);

      ros::spinOnce();
      loop_rate.sleep();
      continue;
    }
    motor_l->setSpeed(l_speed);
    motor_r->setSpeed(r_speed);
    motor_r->getLinearPos();
    motor_l->getLinearPos();

    //std::cout << "Position:" << std::endl << "  " << motor_r->presentLinearPos() << " m" << std::endl;

    double val_r = motor_r->presentLinearPos();
    double val_l = motor_l->presentLinearPos();
    float dist_l = (float)val_l;
    float dist_r = (float)val_r;
    std::vector<float> dists;
    dists.push_back(dist_l);
    dists.push_back(dist_r);

    threemxl::Motor_vals distances;
    distances.distance = dists;

    distancePublisher.publish(distances);

    //dir = -dir;
    ros::spinOnce();
    loop_rate.sleep();
  }

  //motor_l->setTorque(0);
  motor_l->setSpeed(0);
  motor_r->setSpeed(0);
}

void DxlROSMotordriver::commandCallback(const geometry_msgs::Twist::ConstPtr &msg)
{
  float lin_x = msg->linear.x;
  float ang_y = msg->angular.z;

  float l_wheel_lin_speed = lin_x - (ang_y * 54 / 2);
  float r_wheel_lin_speed = lin_x + (ang_y * 54 / 2);
  //l_speed = (l_wheel_lin_speed / 14.5)*-1;
  //r_speed = (r_wheel_lin_speed / 14.5)*-1;
  l_speed = 0.5;
  r_speed = 0.5;

  stop = false;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "dxl_ros_motordriver");

  char *path=NULL;
  if (argc == 2)
    path = argv[1];

  DxlROSMotordriver dxl_motordriver;

  dxl_motordriver.init(path);
  dxl_motordriver.spin();

  return 0;
}
